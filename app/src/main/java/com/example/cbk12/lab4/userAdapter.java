package com.example.cbk12.lab4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by cbk12 on 28.03.2018.
 */

public class userAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> users;
    private LayoutInflater inflater;


    String userName;

    userAdapter(Context context, ArrayList<String> users)
    {
        this.context = context;
        this.users = users;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        View v = inflater.inflate(R.layout.users, viewGroup, false);
        TextView user = v.findViewById(R.id.theuser);
        user.setText(users.get(i));
        user.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        notifyDataSetChanged();
        return v;
    }


}
