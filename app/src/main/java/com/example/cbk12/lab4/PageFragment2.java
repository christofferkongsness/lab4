package com.example.cbk12.lab4;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cbk12 on 28.03.2018.
 */

public class PageFragment2 extends Fragment {

    ListView usersListView;
    private Context context;
    FirebaseDatabase db;
    private String chosenUserName;
    ArrayList<String> users;
    private ArrayList<Message> messages;
    public static boolean usersMsg;

    public static boolean updateFlag;

    Runnable update;
    public PageFragment2()
    {
        db = FirebaseDatabase.getInstance();
        //userName = PreferenceManager.getDefaultSharedPreferences(context).getString("userName", null);
        users = new ArrayList<String>();
        getMessages();
       usersMsg = false;
        updateFlag = false;
        messages = new ArrayList<Message>();
        context = getActivity();

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_user_feed, container, false);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        usersListView = view.findViewById(R.id.listuser);
        usersListView.setAdapter(new userAdapter(context, users));
        usersListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if(!usersMsg)
                {
                    usersMsg = true;
                    chosenUserName = users.get(pos);
                    listInterface();
                    System.out.println("what is chosen User name: "+ chosenUserName);
                }
            }
        });
    }

    private void getMessages() {

        DatabaseReference reference = db.getReference("User");
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String name = (String) dataSnapshot.getValue();
                System.out.println("RECIVED: " + name);
                users.add(name);
                Set<String> hs = new HashSet<>();
                hs.addAll(users);
                users.clear();
                users.addAll(hs);
                Collections.sort(users);
                listInterface();


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void listInterface() {
        if (context != null) {
            if (!usersMsg) {
                usersListView.setAdapter(new userAdapter(context, users));
            } else {
                messages.clear();
                for (Message msg : PageFragment.msgs) {
                    if (msg.getU().equals(chosenUserName)) {
                        messages.add(msg);
                    }
                }
                //messages.add(new Message("10", "checking", "thisshit"));
                usersListView.setAdapter((new Adapter(context, messages)));
            }
        }
    }

}
