package com.example.cbk12.lab4;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by cbk12 on 26.03.2018.
 */

public class FPAdapter extends FragmentPagerAdapter {
    private Context context;
    private String tabTitles[] = new String[] {"Message Feed", "Friends List"};

    private PageFragment page1;
    private PageFragment2 page2;

    private final int PAGECOUNT = 2;

    public FPAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;


        Bundle args = new Bundle();
        page1 = new PageFragment();
        page2 = new PageFragment2();
        page1.page2 = page2;

    }

    @Override
    public Fragment getItem(int position) {
       switch(position)
       {
           case 0: return page1;
           case 1: return page2;
       }
       return null;
    }

    @Override
    public int getCount() {
        return PAGECOUNT;
    }
    @Override
    public CharSequence getPageTitle(int position)
    {
        return tabTitles[position];
    }
}
