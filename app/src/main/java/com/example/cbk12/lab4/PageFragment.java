package com.example.cbk12.lab4;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.TimeZoneNames;
import android.os.AsyncTask;
import android.os.Debug;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;


// In this case, the fragment displays simple text based on the page
public class PageFragment extends Fragment {

    private Context context;
    private ListView msgList;
    FirebaseDatabase db;
    private String userName;
    private DatabaseReference reference;
    public static ArrayList<Message> msgs;
    Adapter adapter;
    boolean newMsg;
    private Handler handler;
    private Runnable task;
    public PageFragment2 page2;

    int delay;

    public PageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseDatabase.getInstance();
        userName = PreferenceManager.getDefaultSharedPreferences(context).getString("userName", null);
        msgs = new ArrayList<Message>();
        getMessages();
        adapter = new Adapter(context, msgs);
        newMsg = false;
        handler = new Handler();




        task  = new Runnable()
        {
            @Override
            public void run()
            {
                setDelay();
                new BackgroundProcess().execute();
                handler.postDelayed(task, 1000* 60 * delay); // time
            }
        };
        task.run();
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.messagefeed, container, false);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        msgList = view.findViewById(R.id.listmsg);
        msgList.setAdapter(adapter);
        Button button = view.findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("is this printed out");

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Enter message!");

                final EditText editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(editText);

                builder.setPositiveButton("Send", new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String msg = editText.getText().toString();
                        Log.d("db", "gethere?");
                        if(!msg.isEmpty())
                        {

                            DatabaseReference myRef = db.getReference("messages");

                            Date date=new Date(System.currentTimeMillis());


                            SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            sfd.setTimeZone(TimeZone.getTimeZone("GMT+0200"));// somehow this works tho im on GMT + 1 not 2;
                            Message newMsg = new Message(sfd.format(date), userName, msg);
                            System.out.println("SEND: d:" + newMsg.getD() + "u: "+ newMsg.getU() + "m: " + newMsg.getM());
                            myRef.push().setValue(newMsg);
                        }
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.show();
            }
        });
    }

    private void getMessages()
    {

        reference = db.getReference("messages");
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message test = dataSnapshot.getValue(Message.class);
                System.out.println("RECIVED: d:" + test.getD() + " u: "+ test.getU() + " m: " + test.getM());
                msgs.add(dataSnapshot.getValue(Message.class));
                sortArray();
                newMsg = true;
                if(context != null) {
                    msgList.setAdapter(new Adapter(context, msgs));

                    if (page2 != null) {
                        page2.listInterface();
                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void sortArray()
    {
        Collections.sort(msgs, new Comparator<Message>()
        {
            @Override
            public int compare(Message message, Message t1) {
                if (message.CalculateDate() == null || t1.CalculateDate() == null) {
                    return 0;
                }
                return t1.CalculateDate().compareTo(message.CalculateDate());
            }

        });
    }

    public void notificationSettings()
    {
        // Create an explicit intent for an Activity in your app

        Intent intent = new Intent(context, chat.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "Lab 4")
                .setSmallIcon(R.drawable.ic_home_black_24dp)
                .setContentTitle(msgs.get(0).getU())
                .setContentText(msgs.get(0).getM())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        int notificationId = 0;
        notificationManager.notify(notificationId, mBuilder.build());

    }

    public class BackgroundProcess extends AsyncTask<Integer, Integer, Exception>   //type accepted, incrementing progress, what is returned
    {
        Exception exception = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Exception doInBackground(Integer... integers)
        {
            if (newMsg)
            {
                notificationSettings();
                newMsg = false;
            }
            return exception;
        }

        @Override
        protected void onPostExecute(Exception s)
        {
            super.onPostExecute(s);
        }
    }

    private void setDelay()
    {
        int[] temp = { 10, 60, 1440};
        delay = temp[(PreferenceManager.getDefaultSharedPreferences(context)).getInt("fetch", 0)];
    }


}