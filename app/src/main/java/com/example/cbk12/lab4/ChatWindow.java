package com.example.cbk12.lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChatWindow extends AppCompatActivity {

    private FirebaseAuth auth;
    private SharedPreferences sharedPreferences;

    private EditText user;
    private FirebaseDatabase db;
    String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_window);
        user = findViewById(R.id.username);
        auth = FirebaseAuth.getInstance();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        db = FirebaseDatabase.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.

        userName = sharedPreferences.getString("userName", null);
        if(userName != null)
        {
            login(false);
        }
    }

    public void register(View view)
    {
        EditText editText = findViewById(R.id.username);
        userName = editText.getText().toString();
        if(!userName.isEmpty()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("userName", userName);
            editor.apply();
            login(true);
        }
    }

    private void login(final boolean first)
    {
        FirebaseUser firebaseUser = auth.getCurrentUser();
        if(firebaseUser == null)
        {
            auth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful())
                    {
                        if(first)
                        {
                            DatabaseReference myRef = db.getReference("User");
                            myRef.push().setValue(userName);

                        }
                        changeView();
                    }
                    else
                    {

                    }
                }
            });
        }
        else
        {
            changeView();
        }
    }

    private void changeView()
    {
        Intent intent = new Intent(this, chat.class).putExtra("userName", userName);
        startActivity(intent);
        finish();
    }



}
