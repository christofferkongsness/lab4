package com.example.cbk12.lab4;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class userPreference extends AppCompatActivity {

    private Spinner fetch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        setContentView(R.layout.activity_user_preference);

        fetch = findViewById(R.id.spinner);
        List<String> list = new ArrayList<>();
        list.add("10 min");
        list.add("1 hour");
        list.add("24 hours");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fetch.setAdapter(adapter);
        fetch.setSelection(getPreference("fetch", settings));

        fetch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("fetch", position);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }
    private int getPreference(String text, SharedPreferences settings)
    {
        return settings.getInt(text, 0);
    }
}
