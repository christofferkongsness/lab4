package com.example.cbk12.lab4;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by cbk12 on 28.03.2018.
 */

public class Adapter extends BaseAdapter {

    private ArrayList<Message> msgs;
    private LayoutInflater inflater;

    Adapter(Context context, ArrayList<Message> msg)
    {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.msgs = msg;

    }

    @Override
    public int getCount() {
        return msgs.size();
    }

    @Override
    public Object getItem(int i) {
        return msgs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Message message = msgs.get(i);
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.textbox, viewGroup, false);
        TextView date = v.findViewById(R.id.dato);
        TextView name = v.findViewById(R.id.name);
        TextView msg = v.findViewById(R.id.messagetext);
        date.setText(message.getD());
        name.setText(message.getU());
        msg.setText(message.getM());
        notifyDataSetChanged();
        return v;
    }
}
