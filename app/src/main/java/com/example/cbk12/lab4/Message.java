package com.example.cbk12.lab4;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by cbk12 on 26.03.2018.
 */



public class Message {

    private String d;
    private String u;
    private String m;

    Message()
    {

    }

    Message(String date, String userName, String message)
    {
        d =date;
        u = userName;
        m = message;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getU() {
        return u;
    }

    public void setU(String u) { this.u = u; }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }



    public Date CalculateDate()
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ParsePosition pos = new ParsePosition(0);

        Date date = null;
        try {
            date = simpleDateFormat.parse(d);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //System.out.println(date.toString());
        return date;
    }
}
