package com.example.cbk12.lab4;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.DialogInterface.*;

public class chat extends AppCompatActivity {

    private FirebaseUser user;
    private String name;
    private FirebaseDatabase db;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        name = getIntent().getStringExtra("userName");
        user = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance();
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        viewPager.setAdapter(new FPAdapter(getSupportFragmentManager(), getApplication()));

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch(id)
        {
            case R.id.UserPreferences:
                Intent intent = new Intent(this, userPreference.class);
                startActivity(intent);
        }


        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();


    }
    @Override
    protected void onStop()
    {
        super.onStop();

    }

    @Override
    public void onBackPressed() {
        if(viewPager.getCurrentItem() == 1) {
            if(PageFragment2.usersMsg)
            {
                PageFragment2.usersMsg = false;
                ((PageFragment2)((FPAdapter)viewPager.getAdapter()).getItem(1)).listInterface();
                return;
            }
        }
        finish();
    }
}
